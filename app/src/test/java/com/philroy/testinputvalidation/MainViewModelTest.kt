package com.philroy.testinputvalidation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.philroy.testinputvalidation.utils.ErrorCases
import com.philroy.testinputvalidation.viewmodels.MainViewModel
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainViewModelTest {
    lateinit var viewModel: MainViewModel
    @get:Rule val instantTaskExecutor = InstantTaskExecutorRule()

    @Before
    fun setup() {
        viewModel = MainViewModel()
    }

    @After
    fun teardown() {

    }

    @Test
    fun `username is blank return error case blank`(){
        // given
        val username = ""
        // when
        viewModel.username = username
        viewModel.validateUserName()
        //then
        assertEquals(ErrorCases.BLANK_FIELD, viewModel.usernameErrorState.value)
    }

    @Test
    fun `username too short returns error case short`(){
        // given
        val username = "alec"
        // when
        viewModel.username = username
        viewModel.validateUserName()
        // then
        assertEquals(ErrorCases.SHORT, viewModel.usernameErrorState.value)
    }

    @Test
    fun `password is valid returns no error`(){
        // given
        val password = "happyfamily4"
        // when
        viewModel.password = password
        viewModel.validatePassword()
        // then
        assertEquals(ErrorCases.NO_ERROR, viewModel.passwordErrorState.value)
    }

    @Test
    fun `password too long returns error case long`(){
        // given
        val password = "happyfamily4happyfamily4"
        // when
        viewModel.password = password
        viewModel.validatePassword()
        // then
        assertEquals(ErrorCases.LONG, viewModel.passwordErrorState.value)
    }

    @Test
    fun `passwords match return error case no error`(){
        // given
        val password = "vulgar_words"
        // when
        viewModel.password = password
        viewModel.confirmPassword = password
        viewModel.validateConfirmPassword()
        // then
        assertEquals(ErrorCases.NO_ERROR, viewModel.confirmPasswordErrorState.value)
    }
}