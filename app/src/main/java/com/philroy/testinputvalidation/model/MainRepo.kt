package com.philroy.testinputvalidation.model

import android.content.UriMatcher.NO_MATCH
import com.philroy.testinputvalidation.utils.ErrorCases

object MainRepo {
    fun validateUserName(username: String): ErrorCases {
        return when {
            (username.isEmpty()) -> {
                ErrorCases.BLANK_FIELD
            }
            (!ALPHABET_ONLY.toRegex().containsMatchIn(username)) -> {
                ErrorCases.INVALID_CHARS
            }
            (username.length < 8) -> {
                ErrorCases.SHORT
            }
            (username.length > 15) -> {
                ErrorCases.LONG
            }
            else -> {
                ErrorCases.NO_ERROR
            }
        }
    }

    fun validatePassword(password: String): ErrorCases {
        return when {
            (password.isEmpty()) -> {
                ErrorCases.BLANK_FIELD
            }
            (FOREIGN_CHAR.toRegex().containsMatchIn(password)) -> {
                ErrorCases.INVALID_CHARS
            }
            (password.length < 5) -> {
                ErrorCases.SHORT
            }
            (password.length > 15) -> {
                ErrorCases.LONG
            }
            else -> {
                ErrorCases.NO_ERROR
            }
        }
    }

    fun validateConfirmPassword(confirmPassword: String, password: String): ErrorCases {
        return when {
            (confirmPassword.isEmpty()) -> {
                ErrorCases.BLANK_FIELD
            }
            (confirmPassword != password) -> {
                ErrorCases.NO_MATCH
            }
            else -> {
                return ErrorCases.NO_ERROR
            }
        }
    }

    private const val FOREIGN_CHAR = "[^\\x00-\\xBE]+"
    private const val ALPHABET_ONLY = "[a-zA-z']+"
}
















