package com.philroy.testinputvalidation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.philroy.testinputvalidation.model.MainRepo
import com.philroy.testinputvalidation.utils.ErrorCases

class MainViewModel: ViewModel() {
    val repo  = MainRepo
    var username: String = ""
    var password: String = ""
    var confirmPassword: String = ""

    private var _usernameErrorState = MutableLiveData<ErrorCases>(ErrorCases.NO_ERROR)
    val usernameErrorState: LiveData<ErrorCases> get() = _usernameErrorState

    private var _passwordErrorState = MutableLiveData<ErrorCases>(ErrorCases.NO_ERROR)
    val passwordErrorState: LiveData<ErrorCases> get() = _passwordErrorState

    private var _confirmPasswordErrorState = MutableLiveData<ErrorCases>(ErrorCases.NO_ERROR)
    val confirmPasswordErrorState: LiveData<ErrorCases> get() = _confirmPasswordErrorState

    fun validateUserName() {
        _usernameErrorState.value = repo.validateUserName(username)
    }

    fun validatePassword() {
        _passwordErrorState.value = repo.validatePassword(password)
    }
    fun validateConfirmPassword() {
        _confirmPasswordErrorState.value = repo.validateConfirmPassword(confirmPassword, password)
    }
}