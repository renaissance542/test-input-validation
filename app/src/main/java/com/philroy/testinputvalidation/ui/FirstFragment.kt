package com.philroy.testinputvalidation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.philroy.testinputvalidation.databinding.FragmentFirstBinding
import com.philroy.testinputvalidation.utils.ErrorCases
import com.philroy.testinputvalidation.viewmodels.MainViewModel

class FirstFragment: Fragment() {
    private var _binding: FragmentFirstBinding? =  null
    private val binding: FragmentFirstBinding get() = _binding!!

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        initViews()
    }

    fun initViews() = with(binding){
        usernameEt.addTextChangedListener {
            viewModel.username = it.toString()
            usernameLayout.isErrorEnabled = false
        }
        passwordEt.addTextChangedListener {
            viewModel.password = it.toString()
            passwordLayout.isErrorEnabled = false
        }
        confirmPasswordEt.addTextChangedListener {
            viewModel.confirmPassword = it.toString()
            confirmPasswordLayout.isErrorEnabled = false
        }

        viewModel.usernameErrorState.observe(viewLifecycleOwner) { errorState ->
            when(errorState) {
                ErrorCases.BLANK_FIELD -> {
                    usernameLayout.error = "Required Field"
                }
                ErrorCases.INVALID_CHARS -> {
                    usernameLayout.error = "Wrong characters"
                }
                ErrorCases.LONG -> {
                    usernameLayout.error = "Toooooooo Looooooonnnnng"
                }
                ErrorCases.SHORT -> {
                    usernameLayout.error = "2 Shrt"
                }
                else -> {

                }
            }
        }

        viewModel.passwordErrorState.observe(viewLifecycleOwner) { errorState ->
            when(errorState) {
                ErrorCases.BLANK_FIELD -> {
                    passwordLayout.error = "Required Field"
                }
                ErrorCases.INVALID_CHARS -> {
                    passwordLayout.error = "Wrong characters"
                }
                ErrorCases.LONG -> {
                    passwordLayout.error = "Toooooooo Looooooonnnnng"
                }
                ErrorCases.SHORT -> {
                    passwordLayout.error = "2 Shrt"
                }
                else -> {

                }
            }
        }

        viewModel.confirmPasswordErrorState.observe(viewLifecycleOwner) { errorState ->
            when(errorState) {
                ErrorCases.NO_MATCH -> {
                    confirmPasswordLayout.error = "Passwords must match"
                }
                ErrorCases.BLANK_FIELD -> {
                    confirmPasswordLayout.error = "Retype password here"
                }
                else -> {

                }
            }
        }

    }

    fun setListeners() = with(binding){
        submitBtn.setOnClickListener {
            viewModel.validateUserName()
            viewModel.validatePassword()
            viewModel.validateConfirmPassword()
            if (viewModel.usernameErrorState.value  == ErrorCases.NO_ERROR &&
                viewModel.passwordErrorState.value  == ErrorCases.NO_ERROR &&
                viewModel.confirmPasswordErrorState.value == ErrorCases.NO_ERROR) {
                val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment()
                findNavController().navigate(action)

            }
        }
    }
}