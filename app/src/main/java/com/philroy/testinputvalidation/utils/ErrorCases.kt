package com.philroy.testinputvalidation.utils

enum class ErrorCases {
    LONG,
    SHORT,
    NO_ERROR,
    INVALID_CHARS,
    BLANK_FIELD,
    NO_MATCH
}